package com.example.android.gimnasio;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.gimnasio.home.DataModel;
import com.example.android.gimnasio.home.HomeAdapter;
import com.example.android.gimnasio.mitra.DataLapanganAdapter;

import java.util.ArrayList;

public class Booked extends Fragment {
    private RecyclerView rv_book;
    private ArrayList<BookedModel> items;
    private BookedAdapter adapter;

    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        items = new ArrayList<BookedModel>();

        rv_book = (RecyclerView) getActivity().findViewById(R.id.rv_book);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getContext());
        BookedAdapter bookedAdapter = new BookedAdapter(items,getActivity());

        rv_book.setLayoutManager(linearLayoutManager1);
        rv_book.smoothScrollToPosition(rv_book.getBottom());
        rv_book.setAdapter(bookedAdapter);
        rv_book.setAdapter(adapter);

        getData();

    }

    private void getData() {
        items = new ArrayList<BookedModel>();
        items.add(new BookedModel("23-09-2019","09.00 - 10.00"));
//        NamaRegist.setText(getTgl());
//        address.setText(book.getAlamatuser());
//        Phone.setText(book.getTeleponuser());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_booked, container, false);
        return view;
    }
}
