package com.example.android.gimnasio;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.android.gimnasio.Maps.Maps;
import com.example.android.gimnasio.home.DataModel;
import com.example.android.gimnasio.home.Home;
import com.example.android.gimnasio.home.HomeAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{
    private Toolbar toolbar;
    private ArrayList<DataModel> mItems;
    private HomeAdapter homeAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadFragment(new Home());
        homeAdapter = new HomeAdapter(this, mItems);
        // inisialisasi BottomNavigaionView
        BottomNavigationView bottomNavigationView = findViewById(R.id.bn_main);
        // beri listener pada saat item/menu bottomnavigation terpilih
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

    }

    private boolean loadFragment(Fragment fragment){
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()){
            case R.id.home:
                fragment = new Home();
                break;
            case R.id.maps:
                fragment = new Maps();
                getSupportActionBar().setTitle("Maps");
                break;
            case R.id.booked:
                fragment = new Booked();
                getSupportActionBar().setTitle("Booked");
                break;
            case R.id.profile:
                fragment = new Profile();
                getSupportActionBar().setTitle("Profile");
                break;

        }
        return loadFragment(fragment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu,menu);
//        MenuItem menuItem = menu.findItem(R.id.action_search);
//        MenuItem notif = menu.findItem(R.id.action_notifikasi);
//        SearchView notifView = (SearchView) menuItem.getActionView();
//        SearchView searchView = (SearchView) notif.getActionView();
//        searchView.setOnQueryTextListener(this);
//        notif.setOnMenuItemClickListener(onNewIntent(new Intent(this,NotificationActivity.class)));
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if (id == R.id.action_notifikasi){
            startActivity(new Intent(this,NotificationActivity.class));
            return true;
        }
        if (id == R.id.action_search){
            Toast.makeText(getApplicationContext(),"search on develop",Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
//    @Override
//    public boolean onQueryTextSubmit(String query) {
//        return false;
//    }
//
//    @Override
//    public boolean onQueryTextChange(String newText) {
//        String userInput = newText.toLowerCase();
//        ArrayList<DataModel> newList = new ArrayList<>();
//
//        for(DataModel name : mItems)
//        {
//            if (name.toString().toLowerCase().contains(userInput))
//            {
//                newList.add(name);
//            }
//        }
//        homeAdapter.updateList(newList);
//        return true;
//    }
}