package com.example.android.gimnasio;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.gimnasio.home.Home;
import com.example.android.gimnasio.mitra.DataLapanganActivity;
import com.example.android.gimnasio.mitra.DataLapanganAdapter;
import com.example.android.gimnasio.mitra.LapanganModel;
import com.example.android.gimnasio.mitra.LoginMitra;
import com.example.android.gimnasio.utils.LocaleHelper;
import com.example.android.gimnasio.utils.UbahBahasaActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static com.google.android.gms.flags.impl.SharedPreferencesFactory.getSharedPreferences;

public class Profile extends Fragment {
    private DatabaseReference reference;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Button btnLogout = getActivity().findViewById(R.id.btnLogout);
        Button btnLoginMitra = getActivity().findViewById(R.id.btnLoginMitra);
        Button btnEdit = getActivity().findViewById(R.id.btnubah);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            getData();
        } else {

        }



        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth auth = FirebaseAuth.getInstance();
                 auth.signOut();
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                v.getContext().startActivity(intent);
                getActivity().finish();

            }
        });
        btnLoginMitra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), LoginMitra.class));
                getActivity().finish();
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), UbahBahasaActivity.class));
                getActivity().finish();
            }
        });
    }

    private void getData() {
        final TextView email = getActivity().findViewById(R.id.tvEmail);
        final TextView nama = getActivity().findViewById(R.id.tvNama);
        final TextView alamat = getActivity().findViewById(R.id.tvalamat);
        final TextView phone = getActivity().findViewById(R.id.tvno);

        final String getEmailuser = email.getText().toString();
        String getNamauser = nama.getText().toString();
        final String getAlamatuser = alamat.getText().toString();
        String getTeleponuser = phone.getText().toString();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        String user_id = auth.getCurrentUser().getUid();
        reference = FirebaseDatabase.getInstance().getReference("User");
        reference.child(user_id)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            //Mapping data pada DataSnapshot ke dalam objek mahasiswa
                            SignupModel profil = snapshot.getValue(SignupModel.class);
                            email.setText(profil.getEmailuser());
                            nama.setText(profil.getNamauser());
                            alamat.setText(profil.getAlamatuser());
                            phone.setText(profil.getTeleponuser());

                            //Mengambil Primary Key, digunakan untuk proses Update dan Delete
                            profil.setKey(snapshot.getKey());
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }
}
