package com.example.android.gimnasio.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.gimnasio.R;

import java.util.ArrayList;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeHolder> {
    private Activity mContext;
    private ArrayList<DataModel> mItems;
    private ArrayList<DataModelKategori> mGor;
    private String category1="";

    public HomeAdapter(final Activity context, final ArrayList<DataModel> items) {
        mContext = context;
        mItems = items;
    }

    @Override
    public HomeHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_categories, parent, false);
        return new HomeHolder(view);
    }

    @Override
    public void onBindViewHolder(final HomeHolder holder, final int position) {
        //holder.getImage().setImageResource(R.drawable.ic_launcher_background);
        //holder.imageView.setImageResource(getImage);
//        final DataModel item = mItems.get(position);
//        holder.getTextView().setText((CharSequence) item);
        //holder.imageView.setImageResource(Integer.parseInt(item.getImage()));
        holder.mTextView.setText(mItems.get(position).getTextView());
        holder.imageView.setImageResource(mItems.get(position).getImage());


    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public Context getContext() {
        return mContext;
    }

    public class HomeHolder extends RecyclerView.ViewHolder {
        public final TextView mTextView;
        public ImageView imageView;

        public HomeHolder(final View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            mTextView = (TextView) itemView.findViewById(R.id.textView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                final int position = getAdapterPosition();

                    switch (position) {
                        case 0:
                            category1="BasketBall";
                            break;
                        case 1:
                            category1="FootBall";
                            break;
                        case 2:
                            category1="TableTennis";
                            break;
                        case 3:
                            category1="Marathon";
                            break;
                        case 4:
                            category1="Badminton";
                            break;
                        case 5:
                            category1="VolleyBall";
                            break;
                    }
                    Intent intent = new Intent(getContext(), BasketActivity.class);
                    intent.putExtra("category", category1);
                    getContext().startActivity(intent);
//                    final String message = String.format(Locale.getDefault(), "Item click at position %d", position);
//                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }

            });
        }

        public void updateList(ArrayList<DataModel> newList) {
            mItems.addAll(newList);
            notifyDataSetChanged();
        }
    }
}