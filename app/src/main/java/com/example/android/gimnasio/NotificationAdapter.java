package com.example.android.gimnasio;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationHolder>  {
    private Activity mContext;
    private ArrayList<Notification> mItems;
    public NotificationAdapter(final Activity context, final ArrayList<Notification> items) {
        mContext = context;
        mItems = items;

    }

    @NonNull
    @Override
    public NotificationHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_notification, viewGroup, false);
        return new NotificationHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationHolder notificationHolder, int position) {
        notificationHolder.tvTitle.setText(mItems.get(position).getTitle());
        notificationHolder.tvDesc.setText(mItems.get(position).getDescription());
        notificationHolder.tvDate.setText(mItems.get(position).getDate());

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class NotificationHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle,tvDesc,tvDate;
        public NotificationHolder(@NonNull View itemView) {

            super(itemView);
            tvTitle = itemView.findViewById(R.id.text_title);
            tvDesc = itemView.findViewById(R.id.text_description);
            tvDate = itemView.findViewById(R.id.text_date);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext,tvTitle.getText().toString(),Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
}
