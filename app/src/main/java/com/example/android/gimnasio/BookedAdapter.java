package com.example.android.gimnasio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.gimnasio.home.BasketActivity;
import com.example.android.gimnasio.home.DataModel;
import com.example.android.gimnasio.home.DataModelKategori;
import com.example.android.gimnasio.home.HomeAdapter;
import com.example.android.gimnasio.mitra.DataLapanganAdapter;
import com.example.android.gimnasio.mitra.LapanganModel;

import java.util.ArrayList;

public class BookedAdapter extends RecyclerView.Adapter<BookedAdapter.ViewHolder> {
    private ArrayList<BookedModel> mItems;
    private Context context;

    public BookedAdapter(ArrayList<BookedModel> items, Context context) {
        this.mItems = items;
        this.context = context;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView name, harga;
        private View container;

        ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            harga = view.findViewById(R.id.harga);
            container = view.findViewById(R.id.card_view);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_booked, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BookedAdapter.ViewHolder viewHolder, int position) {
        final String tgl = mItems.get(position).getTgl();
        final String time = mItems.get(position).getTime();

        viewHolder.name.setText(tgl);
        viewHolder.harga.setText(time);

    }

    @Override
    public int getItemCount() {
        return 0;
    }

}
