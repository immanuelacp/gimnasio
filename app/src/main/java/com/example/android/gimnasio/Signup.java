package com.example.android.gimnasio;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.gimnasio.mitra.AddItemActivity;
import com.example.android.gimnasio.mitra.DataLapanganActivity;
import com.example.android.gimnasio.mitra.LapanganModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Signup extends AppCompatActivity {
    private ArrayList<SignupModel> listUser;
    FirebaseAuth mAuth;
    DatabaseReference getReference;
    String user_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        FirebaseDatabase database = FirebaseDatabase.getInstance();


        final EditText nama = findViewById(R.id.edNameRegist);
        final EditText email = findViewById(R.id.edEmailRegist);
        final EditText pass = findViewById(R.id.edPassRegist);
        final EditText telepon = findViewById(R.id.edNoHP);
        final EditText alamat = findViewById(R.id.enAlamat);
        getReference = FirebaseDatabase.getInstance().getReference("User");
        mAuth = FirebaseAuth.getInstance();


        if (mAuth.getCurrentUser() != null) {
            startActivity(new Intent(Signup.this, booking.class));
            finish();
        }

        init();

    }


    private void init() {
        final EditText nama = findViewById(R.id.edNameRegist);
        final EditText email = findViewById(R.id.edEmailRegist);
        final EditText pass = findViewById(R.id.edPassRegist);
        final EditText telepon = findViewById(R.id.edNoHP);
        final EditText alamat = findViewById(R.id.enAlamat);

        View btnDaftar = findViewById(R.id.btndaftar);

        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DatabaseReference database = FirebaseDatabase.getInstance().getReference("User");
                FirebaseAuth mUser = FirebaseAuth.getInstance();

//                final DatabaseReference getReference = null;

                 final String getNamauser = nama.getText().toString();
                 final String getEmailuser = email.getText().toString();
                final String getPassuser = pass.getText().toString();
                final String getTeleponuser = telepon.getText().toString();
                final String getAlamatuser = alamat.getText().toString();

                if (getNamauser.isEmpty()) {
                    nama.setError("Nama tidak boleh kosong");
                    nama.requestFocus();
                    return;
                }
                else if (getEmailuser.isEmpty()) {
                    email.setError("Email tidak boleh kosong");
                    email.requestFocus();
                    return;
                }
                // jika email not valid
                else if (!Patterns.EMAIL_ADDRESS.matcher(getEmailuser).matches()) {
                    email.setError("Email tidak valid");
                    email.requestFocus();
                    return;
                }
                // jika password kosong
                else if (getPassuser.isEmpty()) {
                    pass.setError("Password tidak boleh kosong");
                    email.requestFocus();
                    return;
                }
                //jika password kurang dari 6 karakter
                else if (getPassuser.length() < 6) {
                    pass.setError("Password minimal terdiri dari 6 karakter");
                    pass.requestFocus();
                    return;
                } else if (getTeleponuser.isEmpty()){
                    telepon.setError("No telepon tidak boleh kosong");
                    telepon.requestFocus();
                    return;
                } else if (getAlamatuser.isEmpty()){
                    alamat.setError("Alamat tidak boleh kosong");
                    alamat.requestFocus();
                    return;
                } else {


                    mAuth.createUserWithEmailAndPassword(email.getText().toString(), pass.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                final String id_user = mAuth.getCurrentUser().getUid();
                                SignupModel user = new SignupModel(id_user,
                                        getNamauser,getEmailuser,getPassuser,getTeleponuser,getAlamatuser
                                );

//                                getReference.child(user_id).setValue(user);

                               database.child(id_user).push()
                                        .setValue(user).addOnSuccessListener(Signup.this, new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        nama.setText("");
                                        email.setText("");
                                        pass.setText("");
                                        telepon.setText("");
                                        alamat.setText("");
                                        Toast.makeText(Signup.this, "Berhasil", Toast.LENGTH_SHORT).show();
                                    }
                                });
//                                FirebaseUser user = mAuth.getCurrentUser();
//                                UserProfileChangeRequest userProfileChangeRequest = new UserProfileChangeRequest.Builder().setDisplayName(nama.getText().toString()).build();
//
//                                user.updateProfile(userProfileChangeRequest);
//                                startActivity(new Intent(Signup.this,booking.class));
//                                finish();
                            }
                        }
                    });
//                    getReference = database.getReference();
//
//                    getReference.child("User").push()
//                            .setValue(new SignupModel(getNamauser, getEmailuser, getPassuser, getTeleponuser, getAlamatuser))
//                            .addOnSuccessListener(Signup.this, new OnSuccessListener<Void>() {
//                                @Override
//                                public void onSuccess(Void aVoid) {
//                                    nama.setText("");
//                                    email.setText("");
//                                    pass.setText("");
//                                    telepon.setText("");
//                                    alamat.setText("");
//                                    Toast.makeText(Signup.this, "Berhasil", Toast.LENGTH_SHORT).show();
//                                }
//                            });

                    startActivity(new Intent(Signup.this, booking.class));
                }

            }
        });

    }



//    public boolean check(){
//        if (nama.getText().toString().equals("")){
//            nama.setError("Masukkan Namamu");
//            nama.requestFocus();
//            return false;
//        }
//        if (email.getText().toString().equals("")){
//            email.setError("Masukkan email");
//            email.requestFocus();
//            return false;
//        }
//
//        if (pass.getText().toString().equals("")){
//            pass.setError("Masukkan password");
//            pass.requestFocus();
//            return false;
//        }
//        return true;
//    }
//
//
//    public void regist(View view){
//        if (check()){
//            mAuth = FirebaseAuth.getInstance();
//            mAuth.createUserWithEmailAndPassword(email.getText().toString(),pass.getText().toString())
//                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//                        @Override
//                        public void onComplete(@NonNull Task<AuthResult> task) {
//                            if (task.isSuccessful()){
//                                FirebaseUser user = mAuth.getCurrentUser();
//                                UserProfileChangeRequest userProfileChangeRequest = new UserProfileChangeRequest.Builder().setDisplayName(nama.getText().toString()).build();
//
//                                user.updateProfile(userProfileChangeRequest);
//                                startActivity(new Intent(Signup.this,booking.class));
//                                finish();
//                            }
//                        }
//                    });
//        }
//    }


}