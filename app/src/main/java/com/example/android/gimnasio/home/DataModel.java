package com.example.android.gimnasio.home;

public class DataModel {
    public String text,category;
    int gambar;

    public DataModel(String t, int gmbr, String category ) {
        this.text = t;
        this.gambar = gmbr;
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTextView() {
        return text;
    }

    public int getImage() {
        return gambar;
    }
}
