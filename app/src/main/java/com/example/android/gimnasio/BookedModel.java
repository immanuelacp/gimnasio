package com.example.android.gimnasio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class BookedModel extends AppCompatActivity {
    public String tgl,time;

    public BookedModel(String tgl, String time) {
        this.tgl = tgl;
        this.time = time;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
