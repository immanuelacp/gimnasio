package com.example.android.gimnasio.utils;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.gimnasio.MainActivity;
import com.example.android.gimnasio.R;

import static com.google.android.gms.flags.impl.SharedPreferencesFactory.getSharedPreferences;

public class UbahBahasaActivity extends AppCompatActivity {
    private String mLanguageCode = "en";
    Switch switch_change_lg;
    TextView tv_bahasa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_preference);
        switch_change_lg = findViewById(R.id.switch_change_lg);
        tv_bahasa = findViewById(R.id.tv_bahasa);
        Button btnSave = findViewById(R.id.buttonSimpan);

        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        switch_change_lg.setChecked(prefs.getBoolean("changeLanguage", switch_change_lg.isChecked()));
        switch_change_lg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                changeLanguage();
            }

        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Language Changed", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(UbahBahasaActivity.this, MainActivity.class));
                recreate();
            }
        });
    }

        public void changeLanguage() {
        SharedPreferences.Editor editor = getSharedPreferences(getPackageName(), MODE_PRIVATE).edit();
        editor.putBoolean("changeLanguage", switch_change_lg.isChecked());
        editor.apply();
        if (switch_change_lg.isChecked()) {
            LocaleHelper.setLocale(UbahBahasaActivity.this, mLanguageCode);
            tv_bahasa.setText(getString(R.string.bahasa));
            recreate();

        } else {
            LocaleHelper.setLocale(this, LocaleHelper.SELECTED_LANGUAGE);
            tv_bahasa.setText(getString(R.string.bahasa));
            recreate();
        }

    }
}
