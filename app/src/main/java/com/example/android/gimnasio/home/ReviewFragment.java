package com.example.android.gimnasio.home;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.gimnasio.R;

public class ReviewFragment extends Fragment {
//    private static final String Aparam1 = "param1";
//    private static final String Aparam2 = "param2";
//    private String mparam1, mparam2;
//    private OnFragmentInteractionListener mlistener;
//
    private AppCompatRatingBar ratingBar;
    private Button btRating;
    private TextView tvRating;
    private Integer intent;

    public ReviewFragment() {
        // Required empty public constructor

//        public static ReviewFragment newInstance(String param1, String param2){
//            ReviewFragment fragment = new ReviewFragment();
//            Bundle args = new Bundle();
//            args.putString(Aparam1, param1);
//            args.putString(Aparam1, param2);
//        }
//        ratingBar = (AppCompatRatingBar) getActivity().findViewById(R.id.rt_bar);
//        btRating = (Button)  getActivity().findViewById(R.id.bt_submit);
//        tvRating = (TextView)  getActivity().findViewById(R.id.tv_rate);
//
//        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
//            @Override
//            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
//                tvRating.setText("Rate : " + rating);
//            }
//        });
//
//        btRating.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(ReviewFragment.this, " Rating " + ratingBar.getRating(), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        new myikc_rating_bar().execute();

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_review, container, false);
    }

    class myikc_rating_bar extends AsyncTask<String, String, String>{
        @Override
        protected String doInBackground(String... args){
            return null;
        }

        protected void onPostExecute(String file) {
            ratingBar = (AppCompatRatingBar) getView().findViewById(R.id.rt_bar);

            tvRating = (TextView) getView().findViewById(R.id.tv_rate);

            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    tvRating.setText("Rate : " + rating);
                }
            });

        }


    }
}
