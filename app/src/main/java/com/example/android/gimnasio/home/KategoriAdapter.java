package com.example.android.gimnasio.home;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.gimnasio.R;

import java.util.ArrayList;
import java.util.Locale;

public class KategoriAdapter extends RecyclerView.Adapter<KategoriAdapter.KategoriHolder> {
    private Context mContext;
    private ArrayList<DataModelKategori> kategoris;

    public KategoriAdapter(ArrayList<DataModelKategori> items, Context context) {
        mContext = context;
        kategoris = items;
    }

    @Override
    public KategoriHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_basket, parent, false);
        return new KategoriHolder(view);
    }

    @Override
    public void onBindViewHolder(final KategoriAdapter.KategoriHolder holder, final int position) {
        //holder.getImage().setImageResource(R.drawable.ic_launcher_background);
        //holder.imageView.setImageResource(getImage);
//        final DataModel item = mItems.get(position);
//        holder.getTextView().setText((CharSequence) item);
        //holder.imageView.setImageResource(Integer.parseInt(item.getImage()));
        final String tvNama = kategoris.get(position).getNamaLap();
        final String tvHarga = kategoris.get(position).getHarga();
        final int imageView = kategoris.get(position).getImage();

        holder.nama.setText(tvNama);
        holder.harga.setText(tvHarga);
        holder.imageView.setImageResource(imageView);

        switch (kategoris.get(position).getCategory()){
            case "BasketBall" :
                holder.imageView.setImageResource(R.drawable.basketlap);
                break;
            case "FootBall" :
                holder.imageView.setImageResource(R.drawable.bola2);
                break;
            case "TableTennis" :
                holder.imageView.setImageResource(R.drawable.tennismeja4);
                break;
            case "Marathon" :
                holder.imageView.setImageResource(R.drawable.run);
                break;
            case "Badminton" :
                holder.imageView.setImageResource(R.drawable.bultang3);
                break;
            case "VolleyBall" :
                holder.imageView.setImageResource(R.drawable.voli3);
                break;
        }

//        holder.nama.setText(kategoris.get(position).getNama());
//        holder.imageView.setImageResource(kategoris.get(position).getImage());
//        holder.harga.setText(kategoris.get(position).getHarga());
    }

    @Override
    public int getItemCount() {
        return kategoris.size();
    }

    public Context getContext() {
        return mContext;
    }

    public class KategoriHolder extends RecyclerView.ViewHolder {
        public TextView nama, harga;
        public ImageView imageView;

        public KategoriHolder(final View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            nama = (TextView) itemView.findViewById(R.id.tv_nama);
            harga = (TextView) itemView.findViewById(R.id.tv_harga);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    final int position = getAdapterPosition();
                    final String message = String.format(Locale.getDefault(), "Item click at position %d", position);
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getContext(), LapanganActivity.class);

                    getContext().startActivity(intent);
                }
            });
        }


        public TextView getNama() {
            return nama;
        }

        public TextView getHarga() {
            return harga;
        }
    }

    public void updateList(ArrayList<DataModelKategori> newList)
    {
        kategoris.addAll(newList);
        notifyDataSetChanged();
    }
}

