package com.example.android.gimnasio;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.android.gimnasio.mitra.DataLapanganActivity;
import com.example.android.gimnasio.mitra.DataLapanganAdapter;
import com.example.android.gimnasio.mitra.LapanganModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;

public class booking extends AppCompatActivity {
    TextView tgg,waktu1,waktu2,NamaRegist,address,Phone,Date,timestart,timeend ;
    private int jam, menit, day, month, year;
    Button checkout, cancel;
    String id_user;
    private DatabaseReference reference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);


       FirebaseAuth mAuth = FirebaseAuth.getInstance();

//       id_user = mAuth.getCurrentUser().getUid();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
          getData();
        } else {
            System.out.println("User not logged in");
        }


        NamaRegist = (TextView) findViewById(R.id.edNameRegist);
        NamaRegist.setText(getIntent().getStringExtra("NamaPelanggan"));

        address = (TextView) findViewById(R.id.address);
        address.setText(getIntent().getStringExtra("Alamat pelanggan"));

        Phone= (TextView) findViewById(R.id.phone);
        Phone.setText(getIntent().getStringExtra("Nomor telpon pelanggan "));

        Date = (TextView) findViewById(R.id.date);
        Date.setText(getIntent().getStringExtra("Tanggal "));

        timestart= (TextView) findViewById(R.id.timestart);
        timestart.setText(getIntent().getStringExtra("Waktu dimulai "));


        timeend= (TextView) findViewById(R.id.timeend);
        timeend.setText(getIntent().getStringExtra("Waktu diakhir"));

        tgg = findViewById(R.id.date);
        tgg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog DatePick = new DatePickerDialog(booking.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        tgg.setText(dayOfMonth + " - " + month + " - " + year);
                    }
                }, year, month, day);
                DatePick.show();
            }
        });

        waktu1 = findViewById(R.id.timestart);
        waktu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                jam = c.get(Calendar.HOUR_OF_DAY);
                menit = c.get(Calendar.MINUTE);

                TimePickerDialog TimePick = new TimePickerDialog(booking.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        waktu1.setText(hourOfDay + " : " + minute);
                    }
                }, jam, menit, true);
                TimePick.show();

            }
        });

        waktu2 = findViewById(R.id.timeend);
        waktu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                jam = c.get(Calendar.HOUR_OF_DAY);
                menit = c.get(Calendar.MINUTE);

                TimePickerDialog TimePick = new TimePickerDialog(booking.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        waktu2.setText(hourOfDay + " : " + minute);
                    }
                }, jam, menit, true);
                TimePick.show();

            }
        });

        checkout = (Button)findViewById(R.id.checkout);
        cancel = (Button)findViewById(R.id.reset);

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama = NamaRegist.getText().toString();
                Intent intent = new Intent(booking.this, checkout.class);
                RadioGroup rg = findViewById(R.id.radiosex);
                RadioButton rb = findViewById(rg.getCheckedRadioButtonId());
                intent.putExtra("NamaPelanggan", NamaRegist.getText().toString());
                intent.putExtra("Alamat pelanggan", address.getText().toString());
                intent.putExtra("TeleponPelanggan", Phone.getText().toString());
                intent.putExtra("Tanggal", Date.getText().toString());
                intent.putExtra("WaktuMulai", timestart.getText().toString());
                intent.putExtra("WaktuAkhir", timeend.getText().toString());
                intent.putExtra("pembayaran", rb.getText().toString());
                booking.this.startActivity(intent);
            }
        });
    }

    private void getData() {
        reference = FirebaseDatabase.getInstance().getReference("User");
        FirebaseAuth auth = FirebaseAuth.getInstance();
        String user_id = auth.getCurrentUser().getUid();
        reference.child(user_id)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                            //Mapping data pada DataSnapshot ke dalam objek mahasiswa
                            SignupModel book = snapshot.getValue(SignupModel.class);
                            NamaRegist.setText(book.getNamauser());
                            address.setText(book.getAlamatuser());
                            Phone.setText(book.getTeleponuser());
                            //Mengambil Primary Key, digunakan untuk proses Update dan Delete
                            book.setKey(snapshot.getKey());
                        }
                        Toast.makeText(getApplicationContext(), "Data Berhasil Dimuat", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

    }

    public void onClick(View v) {
        Intent intent = new Intent(booking.this, checkout.class);
        booking.this.startActivity(intent);
    }

    public void cancel(View view) {
        finish();
    }

//    public void checkout(View view) {
//        Intent intent = new Intent(booking.this, checkout.class);
//        anotherActivityIntent.putExtra("my.package.dataToPass",dataFromClickedRow);
//        startActivity(new Intent(booking.this,checkout.class));
//        finish();
//
//    }
}

