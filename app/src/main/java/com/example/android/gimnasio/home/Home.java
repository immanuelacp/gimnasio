package com.example.android.gimnasio.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.gimnasio.R;

import java.util.ArrayList;

public class Home extends Fragment {
    private ArrayList<DataModel> items;
//    private ArrayList<DataModelKategori> itemsGor;
    private RecyclerView rv_kategori, rv_lapangan;

    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        addDataCategori();
        rv_kategori = (RecyclerView) getActivity().findViewById(R.id.rv_kategori);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getContext());
        HomeAdapter homeAdapter = new HomeAdapter(getActivity(), items);

        rv_kategori.setLayoutManager(linearLayoutManager1);
        rv_kategori.smoothScrollToPosition(rv_kategori.getBottom());
        rv_kategori.setAdapter(homeAdapter);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        rv_kategori.setLayoutManager(manager);

//        addDataGor();
//        rv_lapangan = (RecyclerView) getActivity().findViewById(R.id.rv_lapangan);
//        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getContext());
//        homeAdapter = new HomeAdapter(getContext(), itemsGor);
//
//        rv_kategori.setNestedScrollingEnabled(false);
//        rv_lapangan.setNestedScrollingEnabled(false);
//
//        rv_lapangan.setLayoutManager(linearLayoutManager);
//        rv_lapangan.smoothScrollToPosition(rv_lapangan.getBottom());
//        rv_lapangan.setAdapter(homeAdapter);
//        rv_lapangan.setLayoutManager(manager);


    }

    private void addDataCategori() {
        items = new ArrayList<DataModel>();
        items.add(new DataModel("Basket Ball",R.drawable.basketball1,"BasketBall"));
        items.add(new DataModel("Football",R.drawable.football,"FootBall"));
        items.add(new DataModel("Table Tennis",R.drawable.pingpong,"TableTennis"));
        items.add(new DataModel("Marathon",R.drawable.run,"Marathon"));
        items.add(new DataModel("Badminton",R.drawable.shuttlecock,"Badminton"));
        items.add(new DataModel("Volley Ball",R.drawable.volleyball,"VolleyBall"));
    }

//    private void addDataGor() {
//        items = new ArrayList<DataModelKategori>();
//        items.add(new DataModelKategori("Lapangan 1",R.drawable.basketball1,"Rp.100.000"));
//        items.add(new DataModelKategori("Lapangan 1",R.drawable.basketball1,"Rp.100.000"));
//        items.add(new DataModelKategori("Lapangan 1",R.drawable.basketball1,"Rp.100.000"));
//        items.add(new DataModelKategori("Lapangan 1",R.drawable.basketball1,"Rp.100.000"));
//        items.add(new DataModelKategori("Lapangan 1",R.drawable.basketball1,"Rp.100.000"));
//    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        return view;
    }

    public ActionBar getActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();

    }



}
