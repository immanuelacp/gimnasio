package com.example.android.gimnasio.home;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.gimnasio.Login;
import com.example.android.gimnasio.R;
import com.example.android.gimnasio.Signup;

public class LapanganActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lapangan);

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        // Set the text for each tab.
        tabLayout.addTab(tabLayout.newTab().setText("tab 1"));
        tabLayout.addTab(tabLayout.newTab().setText("ssss"));
        tabLayout.addTab(tabLayout.newTab().setText("sss"));

        ViewPager viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(new LapanganPagerAdapter(getSupportFragmentManager(), 3));
        tabLayout.setupWithViewPager(viewPager);



}
    class LapanganPagerAdapter extends FragmentStatePagerAdapter {
        int mNumTabs;
        public LapanganPagerAdapter(FragmentManager fm, int mNumTabs) {
            super(fm);
            this.mNumTabs = mNumTabs;
        }
        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.field);
                case 1:
                    return getString(R.string.detail);
                case 2:
                    return getString(R.string.review);
                default:
                    return null;

            }
        }
        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new FieldFragment();
                case 1:
                    return new DetailFragment();
                case 2:
                    return new ReviewFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumTabs;
        }
    }

    public void register(View view) {
        startActivity(new Intent(LapanganActivity.this, Login.class));
        finish();
    }
}

