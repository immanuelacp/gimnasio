package com.example.android.gimnasio.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.android.gimnasio.R;

import java.util.ArrayList;

public class Basket extends AppCompatActivity {
    private ArrayList<DataModelKategori> items;
    private String nama;
    private int gambar;
    private RecyclerView rv_basket, rv_lapangan;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_basket);

//        addData();
        rv_basket = findViewById(R.id.rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        KategoriAdapter kategoriAdapter = new KategoriAdapter(items,Basket.this);

        rv_basket.setLayoutManager(linearLayoutManager);
        rv_basket.smoothScrollToPosition(rv_basket.getBottom());
        rv_basket.setAdapter(kategoriAdapter);
        GridLayoutManager manager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        rv_basket.setLayoutManager(manager);
    }

//    private void addData() {
//        items = new ArrayList<>();
//        items.add(new DataModelKategori("Lapangan 1",R.drawable.basketball1,"Rp.100.000"));
//        items.add(new DataModelKategori("Lapangan 1",R.drawable.basketball1,"Rp.100.000"));
//        items.add(new DataModelKategori("Lapangan 1",R.drawable.basketball1,"Rp.100.000"));
//        items.add(new DataModelKategori("Lapangan 1",R.drawable.basketball1,"Rp.100.000"));
//        items.add(new DataModelKategori("Lapangan 1",R.drawable.basketball1,"Rp.100.000"));
//    }

//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_basket, container, false);
//        return view;
//    }

//    public ActionBar getActionBar() {
//        return ((AppCompatActivity) this).getSupportActionBar();
//    }
}
