package com.example.android.gimnasio.mitra;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.gimnasio.R;

import java.util.ArrayList;
import java.util.List;

public class DataLapanganAdapter extends RecyclerView.Adapter<DataLapanganAdapter.ViewHolder> {

    private ArrayList<LapanganModel> lapangan;
    private Context context;
    private Activity activity;
    public DataLapanganAdapter(ArrayList<LapanganModel> lapangan, Context context) {
        this.lapangan = lapangan;
        this.context = context;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView name, harga;
        private View container;
        ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            harga =  view.findViewById(R.id.harga);
            container = view.findViewById(R.id.card_view);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view =LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_mitra, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DataLapanganAdapter.ViewHolder viewHolder, int position) {
//        viewHolder.name.setText(lapangan.get(position).getNamaLap());
//        viewHolder.harga.setText(lapangan.get(position).getHarga());

        final String name = lapangan.get(position).getNamaLap();
        final String harga = lapangan.get(position).getHarga();

        viewHolder.name.setText(name);
        viewHolder.harga.setText(harga);

        switch (lapangan.get(position).getCategory()){
            case "BasketBall" :
                viewHolder.imageView.setImageResource(R.drawable.basketlap);
                break;
            case "FootBall" :
                viewHolder.imageView.setImageResource(R.drawable.bola4);
                break;
            case "TableTennis" :
                viewHolder.imageView.setImageResource(R.drawable.tennismeja4);
                break;
            case "Marathon" :
                viewHolder.imageView.setImageResource(R.drawable.run);
                break;
            case "Badminton" :
                viewHolder.imageView.setImageResource(R.drawable.bultang3);
                break;
            case "VolleyBall" :
                viewHolder.imageView.setImageResource(R.drawable.voli3);
                break;

        }

//        if (lapangan.get(position).getCategory()) {
//            viewHolder.imageView.setImageResource(R.drawable.basketball1);
//        }else {
//            viewHolder.imageView.setImageResource(R.drawable.volleyball);
//        }
//        viewHolder.container.setOnClickListener(onClickListener(position));
    }

    private View.OnClickListener onClickListener(final int position){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,Detail_lapangan_mitra.class);
                intent.putExtra("name", lapangan.get(position).getNamaLap());
                intent.putExtra("harga", lapangan.get(position).getHarga());
                intent.putExtra("desk", lapangan.get(position).getDesk());
                intent.putExtra("cat", lapangan.get(position).getCategory());
                context.startActivity(intent);
            }
        };
    }

    @Override
    public int getItemCount() {
        return lapangan.size();
    }
//    private View.OnClickListener onClickListener(final int position){
//        return new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(activity,Detail_lapangan_mitra.class);
//                intent.putExtra("name", lapangan.get(position).getNamaLap());
//                intent.putExtra("desk", lapangan.get(position).getDesk());
//                intent.putExtra("harga", lapangan.get(position).getHarga());
//                intent.putExtra("category", lapangan.get(position).getCategory());
//                activity.startActivity(intent);
//            }
//        };
//    }




}

