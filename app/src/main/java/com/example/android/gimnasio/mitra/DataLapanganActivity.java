package com.example.android.gimnasio.mitra;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android.gimnasio.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;

public class DataLapanganActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private DataLapanganAdapter adapter;
    private ArrayList<LapanganModel> lapanganArrayList;
    private FloatingActionButton fab;
    private String category;

    //Deklarasi Variable untuk RecyclerView
    private RecyclerView.LayoutManager layoutManager;

    //Deklarasi Variable Database Reference dan ArrayList dengan Parameter Class Model kita.
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_lapangan);

        lapanganArrayList = new ArrayList<>();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);

        addData(); //adding data to array list
        getData();

        recyclerView.setAdapter(adapter);

        fab.setOnClickListener(onAddingListener());

        enabelSwipe();
    }

    private void getData() {
        //Mendapatkan Referensi Database
        reference = FirebaseDatabase.getInstance().getReference();
        reference.child("Lapangan")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Inisialisasi ArrayList
                        lapanganArrayList = new ArrayList<>();

                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            //Mapping data pada DataSnapshot ke dalam objek mahasiswa
                            LapanganModel lapangan = snapshot.getValue(LapanganModel.class);

                            //Mengambil Primary Key, digunakan untuk proses Update dan Delete
                            lapangan.setKey(snapshot.getKey());
                            lapanganArrayList.add(lapangan);
                        }
                        adapter = new DataLapanganAdapter(lapanganArrayList, DataLapanganActivity.this);

                        //Memasang Adapter pada RecyclerView
                        recyclerView.setAdapter(adapter);

                        Toast.makeText(getApplicationContext(), "Data Berhasil Dimuat", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void addData() {
        lapanganArrayList.add(new LapanganModel("Lp Basket 1", "Rp. 200.000", "Lapangan basket yang nyaman", "BasketBall"));
    }

    private View.OnClickListener onAddingListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                final Dialog dialog = new Dialog(DataLapanganActivity.this);
//                Window dialogWindow = dialog.getWindow();
//                WindowManager.LayoutParams lp = dialogWindow.getAttributes();
//                dialogWindow.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL);
//
//                dialog.setContentView(R.layout.item_dialog_mitra);
//                dialog.setTitle("Add a new data");
//                dialog.setCancelable(false);
//                lp.width = 1000; // Width
//                lp.height = 1000; // Height
//                dialogWindow.setAttributes(lp);
//                Spinner spnCat = (Spinner) dialog.findViewById(R.id.category);
//                EditText name = (EditText) dialog.findViewById(R.id.name);
//                EditText harga = (EditText) dialog.findViewById(R.id.harga);
//                EditText desk = (EditText) dialog.findViewById(R.id.desk);
//
//                View btnAdd = dialog.findViewById(R.id.btn_ok);
//                View btnCancel = dialog.findViewById(R.id.btn_cancel);
//
//                //set spinner adapter
//                ArrayList<String> catList = new ArrayList<>();
//                catList.add("BasketBall");
//                catList.add("Football");
//                catList.add("TableTennis");
//                catList.add("Marathon");
//                catList.add("Badminton");
//                catList.add("VolleyBall");
//                ArrayAdapter<String> spnAdapter = new ArrayAdapter<String>(DataLapanganActivity.this,
//                        android.R.layout.simple_dropdown_item_1line, catList);
//                spnCat.setAdapter(spnAdapter);
//
//                spnCat.setOnItemSelectedListener(onItemSelectedListener());
//                btnAdd.setOnClickListener(onConfirmListener(name, harga, desk, dialog));
//                btnCancel.setOnClickListener(onCancelListener(dialog));
//                dialog.show();
                startActivity(new Intent(DataLapanganActivity.this, AddItemActivity.class));
            }
        };
    }

    private AdapterView.OnItemSelectedListener onItemSelectedListener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        category ="Basketball";
                        break;
                    case 1 :
                        category="FootBall";
                        break;
                    case 2 :
                        category="TableTennis";
                        break;
                    case 3 :
                        category="Marathon";
                        break;
                    case 4 :
                        category="Badminton";
                        break;
                    case 5 :
                        category="VolleyBall";
                        break;

                }
//                if (position == 0) {
//                    category = true;
//                } else {
//                    category = false;
//                }
            }

            @Override
            public void onNothingSelected(AdapterView parent) {
            }
        };
    }

//    private View.OnClickListener onConfirmListener(final EditText name, final EditText harga, final EditText desk, final Dialog dialog) {
//        return new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                LapanganModel dataNew = new LapanganModel(name.getText().toString().trim(), harga.getText().toString().trim(), desk.getText().toString().trim());
//                lapanganArrayList.add(dataNew);
//                adapter.notifyDataSetChanged();
//                dialog.dismiss();
//            }
//        };
//    }

    private View.OnClickListener onCancelListener(final Dialog dialog) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };
    }

    private void enabelSwipe() {
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT){

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int position) {
                position = viewHolder.getAdapterPosition();
                lapanganArrayList.remove(position);
                adapter.notifyItemRemoved(position);
                adapter.notifyItemRangeChanged(position,adapter.getItemCount());
                Toast.makeText(getApplicationContext(),"Deleted",Toast.LENGTH_SHORT).show();
            }
        };
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
    }
}
