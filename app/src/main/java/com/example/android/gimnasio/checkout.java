package com.example.android.gimnasio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class checkout extends AppCompatActivity {

//    String NamaRegist,address,Phone,Date,timestart,timeend ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);


//        Intent in = getIntent();
        TextView NamaRegist = (TextView) findViewById(R.id.edNameRegist);
        NamaRegist.setText(getIntent().getStringExtra("NamaPelanggan"));

        TextView address = (TextView) findViewById(R.id.address);
        address.setText(getIntent().getStringExtra("Alamat pelanggan"));

        TextView Phone = (TextView) findViewById(R.id.phone);
        Phone.setText(getIntent().getStringExtra("TeleponPelanggan"));

        TextView Date = (TextView) findViewById(R.id.date);
        Date.setText(getIntent().getStringExtra("Tanggal"));

        TextView timestart = (TextView) findViewById(R.id.time);
        timestart.setText(getIntent().getStringExtra("WaktuMulai") + " - " + getIntent().getStringExtra("WaktuAkhir"));

        TextView metode = (TextView) findViewById(R.id.metode);
        metode.setText(getIntent().getStringExtra("pembayaran"));

    }

    public void bukadialog(View view) {
        startActivity(new Intent(this, dialog.class));
    }
}

