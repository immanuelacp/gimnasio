package com.example.android.gimnasio;

public class SignupModel {
    private String namauser, emailuser, passuser, teleponuser, alamatuser, key;
    String user_id;

    public SignupModel(){

    }

    public String getKey(){
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }

    public String getNamauser(){
        return namauser;
    }

    public void setNamauser(String nama){
        this.namauser = nama;
    }

    public String getEmailuser(){
        return  emailuser;
    }

    public void setEmailuser(String email){
        this.emailuser = email;
    }

    public String getPassuser(){
        return  passuser;
    }

    public void setPassuser(String pass){
        this.passuser = pass;
    }

    public String getTeleponuser(){
        return  teleponuser;
    }

    public void setTeleponuser(String telepon){
        this.teleponuser = telepon;
    }

    public String getAlamatuser(){
        return  alamatuser;
    }

    public void setAlamatuser(String alamat){
        this.alamatuser = alamat;
    }


    public SignupModel(String user_id, String namauser, String emailuser, String passuser, String teleponuser, String alamatuser ){
        this.namauser = namauser;
        this.emailuser = emailuser;
        this.user_id = user_id;
        this.passuser = passuser;
        this.teleponuser = teleponuser;
        this.alamatuser = alamatuser;
    }

    public String getUser_id(){
        return user_id;
    }

}
