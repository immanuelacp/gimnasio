package com.example.android.gimnasio.mitra;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android.gimnasio.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class AddItemActivity extends AppCompatActivity {
    private ArrayList<LapanganModel> lapanganArrayList;
    private DataLapanganAdapter adapter;
    private String category;
    private DatabaseReference database;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        Spinner spnCat = (Spinner) findViewById(R.id.category);
        final EditText nameLap = (EditText) findViewById(R.id.name);
        final EditText harga = (EditText) findViewById(R.id.harga);
        final EditText desk = (EditText) findViewById(R.id.desk);

        init();
        lapanganArrayList = new ArrayList<>();
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {

        }
        database = FirebaseDatabase.getInstance().getReference();
    }


    private void init() {
        final Spinner spnCat = (Spinner) findViewById(R.id.category);
        final EditText namaLap = (EditText) findViewById(R.id.name);
        final EditText harga = (EditText) findViewById(R.id.harga);
        final EditText desk = (EditText) findViewById(R.id.desk);

        View btnAdd = findViewById(R.id.btn_add);
        View btnCancel = findViewById(R.id.btn_cancel);

        //set spinner adapter
        ArrayList<String> catList = new ArrayList<>();
        catList.add("BasketBall");
        catList.add("Football");
        catList.add("TableTennis");
        catList.add("Marathon");
        catList.add("Badminton");
        catList.add("VolleyBall");
        ArrayAdapter<String> spnAdapter = new ArrayAdapter<String>(AddItemActivity.this,
                android.R.layout.simple_dropdown_item_1line, catList);
        spnCat.setAdapter(spnAdapter);

        spnCat.setOnItemSelectedListener(onOptionsItemSelected());

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference getReference;
                String getNamaLap = namaLap.getText().toString();
                String getHarga = harga.getText().toString();
                String getDesk = desk.getText().toString();
                String getCategory = spnCat.getSelectedItem().toString();

                getReference = database.getReference();
                getReference.child("Lapangan").push()
                        .setValue(new LapanganModel(getNamaLap, getHarga, getDesk, getCategory))
                        .addOnSuccessListener(AddItemActivity.this, new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                namaLap.setText("");
                                harga.setText("");
                                desk.setText("");
                                Toast.makeText(AddItemActivity.this, "Data berhasil ditambah", Toast.LENGTH_SHORT).show();
                            }
                        });
//                LapanganModel dataNew = new LapanganModel(name.getText().toString().trim(), harga.getText().toString().trim(), desk.getText().toString().trim());
//                lapanganArrayList.add(dataNew);
                startActivity(new Intent(AddItemActivity.this,DataLapanganActivity.class));
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddItemActivity.this, DataLapanganActivity.class));
            }
        });

    }

    private AdapterView.OnItemSelectedListener onOptionsItemSelected() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        category ="Basketball";
                        break;
                    case 1 :
                        category="FootBall";
                        break;
                    case 2 :
                        category="TableTennis";
                        break;
                    case 3 :
                        category="Marathon";
                        break;
                    case 4 :
                        category="Badminton";
                        break;
                    case 5 :
                        category="VolleyBall";
                        break;

                }
//                String selected = parent.getItemAtPosition(position).toString();
//                if (position == 0) {
//                    ;
//                } else {
//                    category = false;
//                }
            }

            @Override
            public void onNothingSelected(AdapterView parent) {
            }
        };
    }


}
