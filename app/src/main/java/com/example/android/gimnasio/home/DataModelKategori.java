package com.example.android.gimnasio.home;

public class DataModelKategori {
    public String namaLap, harga, category,key;
    int gambar;

    public DataModelKategori() {

    }
    public String getKey(){
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }

    public String getNamaLap() {
        return namaLap;
    }
    public void setNamaLap(String nama){
        this.namaLap = nama;
    }

    public String getHarga() {
        return harga;
    }
    public void setHarga(String harga){
        this.harga=harga;
    }

    public int getImage() {
        return gambar;
    }
    public void setImage(int gambar){
        this.gambar=gambar;
    }
    public String getCategory(){
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }

    public DataModelKategori(String nama, int gambar, String harga, String category) {
        this.namaLap = nama;
        this.gambar = gambar;
        this.harga = harga;
        this.category=category;
    }
}
