package com.example.android.gimnasio.mitra;

public class LapanganModel {
    private String namaLap, desk, harga, key;
    private String category;

    public LapanganModel(){

    }

    public String getKey(){
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }

    public String getNamaLap(){
        return namaLap;
    }

    public void setNamaLap(String nama){
        this.namaLap = nama;
    }

    public String getDesk(){
        return  desk;
    }

    public void setDesk(String desk){
        this.desk = desk;
    }

    public String getHarga(){
        return  harga;
    }

    public void setHarga(String harga){
        this.harga = harga;
    }

    public String getCategory(){
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }

    public LapanganModel(String namaLap, String harga, String desk, String category ){
        this.namaLap = namaLap;
        this.harga = harga;
        this.desk = desk;
        this.category = category;
    }
}
