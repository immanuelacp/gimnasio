package com.example.android.gimnasio;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.gimnasio.mitra.LapanganModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Login extends AppCompatActivity {
    FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

        final EditText email = findViewById(R.id.edmailLogin);
        final EditText pass = findViewById(R.id.edPassLogin);

        mAuth = FirebaseAuth.getInstance();
                FirebaseUser user = mAuth.getCurrentUser();
                if (user != null) {
                    Intent intent = new Intent(Login.this, booking.class);
                    startActivity(intent);
                } else {
                    System.out.println("User not logged in");
                }
    }

        public void onStop(){
            super.onStop();
            if (mAuthListener != null) {
                mAuth.removeAuthStateListener(mAuthListener);

            }
        }

    private void init() {
        final EditText email = findViewById(R.id.edmailLogin);
        final EditText pass = findViewById(R.id.edPassLogin);
        View btnLogin = findViewById(R.id.loginBtn);
        final TextView regist = findViewById(R.id.textView9);

//        regist.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(Login.this,Signup.class));
//                finish();
//            }
//        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference getReference;
                String getEmailuser = email.getText().toString();
                String getPassuser = pass.getText().toString();

                if (getEmailuser.isEmpty()) {
                    email.setError("Email tidak boleh kosong");
                }
                // jika email not valid
                else if (!Patterns.EMAIL_ADDRESS.matcher(getEmailuser).matches()) {
                    email.setError("Email tidak valid");
                }
                // jika password kosong
                else if (getPassuser.isEmpty()) {
                    pass.setError("Password tidak boleh kosong");
                }
                //jika password kurang dari 6 karakter
                else if (getPassuser.length() < 6) {
                    pass.setError("Password minimal terdiri dari 6 karakter");
                } else {
                    getReference = database.getReference();

                    getReference.child("User").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                //Mapping data pada DataSnapshot ke dalam objek mahasiswa
                                SignupModel login = snapshot.getValue(SignupModel.class);

                                //Mengambil Primary Key, digunakan untuk proses Update dan Delete
                                login.setKey(snapshot.getKey());
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }

                    });


                    startActivity(new Intent(Login.this, booking.class));
                }

//                if (mAuth.getCurrentUser() != null) {
//                    startActivity(new Intent(Login.this, booking.class));
//                    finish();
//                }

//    public boolean check(){
//        if (email.getText().toString().equals("")){
//            email.setError("Isi Email");
//            email.requestFocus();
//            return false;
//        }
//        if (pass.getText().toString().equals("")){
//            pass.setError("Isi Password dulu");
//            pass.requestFocus();
//            return false;
//        }
//        return true;
//    }
//
//    public void login(View view){
//        if (check()) {
//            new AsyncTask<Void,Void,Boolean>(){
//                @Override
//                protected Boolean doInBackground(Void... voids) {
//                    mAuth.signInWithEmailAndPassword(email.getText().toString(), pass.getText().toString())
//                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//                                @Override
//                                public void onComplete(@NonNull Task<AuthResult> task) {
//                                    if (task.isSuccessful()) {
//                                        startActivity(new Intent(Login.this, booking.class));
//                                        finish();
//                                    } else {
//                                        Toast.makeText(Login.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//                            });
//                    return null;
//                }
//
//                @Override
//                protected void onPreExecute() {
//                    Toast.makeText(Login.this, "Sign In...", Toast.LENGTH_SHORT).show();
//                }
//
//                @Override
//                protected void onPostExecute(Boolean aBoolean) {
//                    super.onPostExecute(aBoolean);
//                }
//            }.execute();
//        }
//    }
            }

        });

    }
    public void regist(View view) {
        startActivity(new Intent(Login.this, Signup.class));
        finish();
    }
}
