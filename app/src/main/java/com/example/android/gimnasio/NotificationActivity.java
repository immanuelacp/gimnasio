package com.example.android.gimnasio;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private NotificationAdapter adapter;
    private ArrayList<Notification> items;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        addNotif();
        recyclerView = (RecyclerView) findViewById(R.id.rv_notification);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getApplicationContext());

        NotificationAdapter notificationAdapter = new NotificationAdapter(this,items);
        recyclerView.setAdapter(notificationAdapter);
        recyclerView.setLayoutManager(linearLayoutManager1);

    }

    private void addNotif() {
        items = new ArrayList<Notification>();
        items.add(new Notification("Notif 1","Ini notif 1","08/05/1998","10.20"));
    }
}
