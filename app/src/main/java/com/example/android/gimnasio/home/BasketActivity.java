package com.example.android.gimnasio.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.android.gimnasio.NotificationActivity;
import com.example.android.gimnasio.R;
import com.example.android.gimnasio.booking;
import com.example.android.gimnasio.checkout;
import com.example.android.gimnasio.mitra.DataLapanganActivity;
import com.example.android.gimnasio.mitra.DataLapanganAdapter;
import com.example.android.gimnasio.mitra.LapanganModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class BasketActivity extends Activity {
    private ArrayList<DataModelKategori> items;
    private RecyclerView rv;
    private DatabaseReference reference;
    private GorAdapter adapter;
    String category1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_basket);

        items = new ArrayList<>();
        getdata();
        rv = findViewById(R.id.rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        GorAdapter adapter = new GorAdapter(items, BasketActivity.this);

        rv.setLayoutManager(linearLayoutManager);
        rv.smoothScrollToPosition(rv.getBottom());
        rv.setAdapter(adapter);
        GridLayoutManager manager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        rv.setLayoutManager(manager);
        Bundle bundle = getIntent().getExtras();
        category1 = bundle.getString("category");
    }

    private void getdata() {
        reference = FirebaseDatabase.getInstance().getReference();
        reference.child("Lapangan")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Inisialisasi ArrayList
                        items = new ArrayList<>();

                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            //Mapping data pada DataSnapshot ke dalam objek mahasiswa
                            DataModelKategori cat = snapshot.getValue(DataModelKategori.class);

                            //Mengambil Primary Key, digunakan untuk proses Update dan Delete
                            cat.setKey(snapshot.getKey());
                            if (snapshot.child("category").exists()) {
                                if (snapshot.child("category").getValue().toString().equals(category1)) {
                                    items.add(cat);
                                }
                            }
                        }
                        adapter = new GorAdapter(items, BasketActivity.this);

                        //Memasang Adapter pada RecyclerView
                        rv.setAdapter(adapter);

                        Toast.makeText(getApplicationContext(), "Data Berhasil Dimuat", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu,menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
//        searchView.setOnQueryTextListener(this);
        return true;
    }

    public void notif(MenuItem item) {
        startActivity(new Intent(this, NotificationActivity.class));
    }
}
