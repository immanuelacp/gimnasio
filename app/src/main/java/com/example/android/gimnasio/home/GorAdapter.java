package com.example.android.gimnasio.home;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.gimnasio.R;

import java.util.ArrayList;
import java.util.Locale;

public class GorAdapter extends RecyclerView.Adapter<GorAdapter.GorHolder>{
    private Context mContext;
    private ArrayList<DataModelKategori> mItems;

    public GorAdapter(ArrayList<DataModelKategori> items, Context context) {
        mContext = context;
        mItems = items;
    }

    @Override
    public GorHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_lapangan, parent, false);
        return new GorHolder(view);
    }

    @Override
    public void onBindViewHolder(final GorHolder holder, final int position) {
        //holder.getImage().setImageResource(R.drawable.ic_launcher_background);
        //holder.imageView.setImageResource(getImage);
//        final DataModel item = mItems.get(position);
//        holder.getTextView().setText((CharSequence) item);
        //holder.imageView.setImageResource(Integer.parseInt(item.getImage()));

        final String tvNama = mItems.get(position).getNamaLap();
        final String tvHarga = mItems.get(position).getHarga();
        final int imageView = mItems.get(position).getImage();

        holder.tvNama.setText(tvNama);
        holder.tvHarga.setText(tvHarga);
        holder.imageView.setImageResource(imageView);

        switch (mItems.get(position).getCategory()){
            case "BasketBall" :
                holder.imageView.setImageResource(R.drawable.basketlap);
                break;
            case "FootBall" :
                holder.imageView.setImageResource(R.drawable.bola2);
                break;
            case "TableTennis" :
                holder.imageView.setImageResource(R.drawable.tennismeja4);
                break;
            case "Marathon" :
                holder.imageView.setImageResource(R.drawable.run);
                break;
            case "Badminton" :
                holder.imageView.setImageResource(R.drawable.bultang3);
                break;
            case "VolleyBall" :
                holder.imageView.setImageResource(R.drawable.voli3);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public Context getContext() {
        return mContext;
    }

    class GorHolder extends RecyclerView.ViewHolder {
        private TextView tvNama,tvHarga;
        private ImageView imageView;
        private View container;

        GorHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            tvNama =  itemView.findViewById(R.id.tv_nama);
            tvHarga =  itemView.findViewById(R.id.tv_harga);
            container = itemView.findViewById(R.id.cardView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    final int position = getAdapterPosition();
//                    Fragment basket = new Fragment();
//                    FragmentManager fragmentManager = getFragmentManager();
//                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(android.R.id.content, basket);
//                    fragmentTransaction.commit();
//                    loadFragment(new Basket());
                    final String message = String.format(Locale.getDefault(), "Item click at position %d", position);
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getContext(), LapanganActivity.class);
                    getContext().startActivity(intent);

                }

            });
        }



    }

    public void updateList(ArrayList<DataModelKategori> newList)
    {
        mItems.addAll(newList);
        notifyDataSetChanged();
    }
}
